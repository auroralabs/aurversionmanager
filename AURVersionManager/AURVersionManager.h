//
//  AURVersionManager.h
//
//  Created by Álvaro Murillo del Puerto on 9/3/17.
//  Copyright © 2017 Auroralabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AURVersionManager : NSObject

@property (nonatomic, readonly) NSString *previousVersion;

@property (nonatomic, readonly) NSString *currentVersion;

@property (nonatomic, readonly) BOOL isFirstInstall;

@property (nonatomic, readonly) BOOL isAnUpdate;

@property (nonatomic, readonly) NSInteger launchesCount;

@property (nonatomic, readonly) NSDate *firstLaunch;

@property (nonatomic, readonly) NSArray *installedVersions;

+ (instancetype)sharedInstance;

@end
