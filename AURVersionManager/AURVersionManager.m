//
//  AURVersionManager.m
//
//  Created by Álvaro Murillo del Puerto on 9/3/17.
//  Copyright © 2017 Auroralabs. All rights reserved.
//

#import "AURVersionManager.h"

static NSString * PREVIOUS_VERSION_KEY = @"vm_previous_version";
static NSString * CURRENT_VERSION_KEY = @"vm_current_version";
static NSString * FIRST_LAUNCH_KEY = @"vm_first_launch";
static NSString * LAUNCHES_COUNT_KEY = @"vm_launches_count";
static NSString * INSTALLED_VERSIONS_KEY = @"vm_installed_versions";

@interface AURVersionManager ()

@property (nonatomic, assign) BOOL isFirstInstall;
@property (nonatomic, assign) BOOL isAnUpdate;

@end

@implementation AURVersionManager

+ (instancetype)sharedInstance {
    
    static dispatch_once_t onceQueue;
    static AURVersionManager *instance = nil;
    
    dispatch_once(&onceQueue, ^{ instance = [[self alloc] init]; });
    return instance;
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        [self loadUpdatesInfo];
    }
    
    return self;
}

- (void)loadUpdatesInfo {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Versions
    NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *previousVersion = [userDefaults objectForKey:CURRENT_VERSION_KEY];
    
    [userDefaults setObject:currentVersion forKey:CURRENT_VERSION_KEY];
    
    
    // Launches
    NSDate *currentLaunchDate = [NSDate date];
    
    NSInteger launchesCount = [userDefaults integerForKey:LAUNCHES_COUNT_KEY] + 1;    
    [userDefaults setInteger:launchesCount forKey:LAUNCHES_COUNT_KEY];
    
    if (!previousVersion) {
        
        // First install
        _isFirstInstall = YES;
        
        [userDefaults setObject:currentLaunchDate forKey:FIRST_LAUNCH_KEY];
        [userDefaults setObject:@[currentVersion] forKey:INSTALLED_VERSIONS_KEY];
        
    } else if (![previousVersion isEqualToString:currentVersion]) {
        
        // The user updates the app
        _isAnUpdate = YES;
        
        [userDefaults setObject:previousVersion forKey:PREVIOUS_VERSION_KEY];
        
        NSArray *installedVersions = [userDefaults objectForKey:INSTALLED_VERSIONS_KEY];
        [userDefaults setObject:[installedVersions arrayByAddingObject:currentVersion] forKey:INSTALLED_VERSIONS_KEY];        
    }
    
    
    [userDefaults synchronize];
}

- (NSString *)currentVersion {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_VERSION_KEY];
}

- (NSString *)previousVersion {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:PREVIOUS_VERSION_KEY];
}

- (NSInteger)launchesCount {
    
    return [[NSUserDefaults standardUserDefaults] integerForKey:LAUNCHES_COUNT_KEY];
}

- (NSDate *)firstLaunch {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_LAUNCH_KEY];
}

- (NSArray *)installedVersions {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:INSTALLED_VERSIONS_KEY];
}

- (NSString *)description {
    
    NSMutableArray *lines = [NSMutableArray new];
    
    if (self.isFirstInstall) {
        [lines addObject:@"It's first install!"];
    } else if (self.isAnUpdate) {
        [lines addObject:@"It's an update!"];
    } else {
        [lines addObject:@"It's the same version than previous launch."];
    }
    
    [lines addObject:[NSString stringWithFormat:@"Current version: %@", self.currentVersion]];
    
    if (self.previousVersion) {
        [lines addObject:[NSString stringWithFormat:@"Previous version: %@", self.previousVersion]];
    }
    
    [lines addObject:[NSString stringWithFormat:@"First Launch: %@", self.firstLaunch]];
    
    [lines addObject:[NSString stringWithFormat:@"App launches: %ld", self.launchesCount]];
    
    [lines addObject:[NSString stringWithFormat:@"Installed versions: %@", [self.installedVersions componentsJoinedByString:@", "]]];
    
    return [lines componentsJoinedByString:@"\n"];
}

@end
