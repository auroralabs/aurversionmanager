> **AVISO:** este proyecto está desactualizado. Todo su contenido ha sido integrado en https://bitbucket.org/auroralabs/ios-architecture

# AURVersionManager #

Librería para llevar el control de las versiones instaladas por el usuario, detectar si es la primera instalación o saber sobre que versión está actualizando.

### Instalación ###

Añade el siguiente pod a tu proyecto

```ruby
pod 'AURVersionManager', :git => 'https://bitbucket.org/auroralabs/aurversionmanager.git', :tag => '1.0.0'
```

### Uso ###

```objective-c
#import <AURVersionManager/AURVersionManager.h>
```

```objective-c

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    AURVersionManager *versionManager = [AURVersionManager sharedInstance];
    
    if (versionManager.isFirstInstall) {
        
        NSLog(@"First Install");
        
    } else if (versionManager.isAnUpdate) {
        
        NSLog(@"Update from %@ to %@", versionManager.previousVersion, versionManager.currentVersion);
        
    } else {
        
        NSLog(@"Launches %ld", versionManager.launchesCount);
    }
    
    return YES;
}
```