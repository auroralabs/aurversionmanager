//
//  ViewController.m
//  Sample
//
//  Created by Álvaro Murillo del Puerto on 9/3/17.
//  Copyright © 2017 Auroralabs. All rights reserved.
//

#import "ViewController.h"
#import <AURVersionManager/AURVersionManager.h>


@interface ViewController ()

@property (nonatomic, strong) IBOutlet UILabel *versionLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.versionLabel.text = [AURVersionManager sharedInstance].description;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
