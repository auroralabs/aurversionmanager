//
//  AppDelegate.h
//  Sample
//
//  Created by Álvaro Murillo del Puerto on 9/3/17.
//  Copyright © 2017 Auroralabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

