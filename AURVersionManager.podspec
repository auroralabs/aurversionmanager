Pod::Spec.new do |s|
  s.name     = 'AURVersionManager'
  s.version  = '1.0.0'
  s.license  = 'MIT'
  s.summary  = 'Control the versions of your app the user install'
  s.homepage = 'https://bitbucket.org/auroralabs/aurversionmanager'
  s.author  = 'Álvaro Murillo'
  s.source   = { :git => 'https://alvaromurillo@bitbucket.org/auroralabs/aurversionmanager.git', :tag => s.version }
  s.requires_arc = true
  
  s.public_header_files = 'AURVersionManager/AURVersionManager.h'
  s.source_files = 'AURVersionManager/AURVersionManager.{h,m}'
end
